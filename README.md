# version-compare-utility



## Description

This project supports to get the version comparison reports across the IDP brands based on requirement.

## How to Start

Run as springboot application

## APIs

API to downalod Version Comparison Report - BWW vs ARB vs SNC (UAT & STG)

GET : http://localhost:8080/api/version-compare/uat-vs-stg

Hit this API in your browser to download the report in excel format.

## Authors and acknowledgment
HSC Domain Services Team