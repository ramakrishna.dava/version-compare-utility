package com.inspirebrands.version.compare.controller;

import com.inspirebrands.version.compare.service.VersionCompareService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;

@AllArgsConstructor
@RestController
@RequestMapping("/api/version-compare")
public class VersionCompareController {

    private final VersionCompareService versionCompareService;

    //This API will return Excel file with version comparison between uat & stg for all the brands (bww,arb,snc)
    @GetMapping("/uat-vs-stg")
    public ResponseEntity<byte[]> getUatVsStgComparisonReport() {

        ByteArrayOutputStream outputStream = versionCompareService.getVersionComparisonReport();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "VersionCompareReport_UatVsStg.xlsx");

        return ResponseEntity.ok()
                .headers(headers)
                .body(outputStream.toByteArray());
    }
}