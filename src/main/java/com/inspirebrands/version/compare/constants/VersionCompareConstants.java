package com.inspirebrands.version.compare.constants;

import java.util.List;

public class VersionCompareConstants {
    public static final String ACCOUNT_LOWER = "3575391";
    public static final String ACCOUNT_PROD = "2879290";
    public static final String ENCODED_KEY_LOWER = "TlJJUS10YVB6blVkRFNITGM4czBPQ1BPS012VXh0WkhQZUtJZg==";
    public static final String ENCODED_KEY_PROD = "TlJJUS1UME00TUJnWnlBSFMwZm02eDhDV0hEemNub1VPS2dmSQ==";
    public static final String APP_NAME_HEADER = "App Name";
    public static final CharSequence STG01 = "stg01";
    public static final CharSequence PRD01 = "prd01";
    public static final int THREAD_POOL_SIZE = 6;
    public static final String VERSION_COMPARE = "VersionCompare";
    public static final String ARB_UAT01 = "arb-uat01";
    public static final String ARB_STG01 = "arb-qa01";
    public static final String BWW_UAT02 = "bww-uat02";
    public static final String BWW_STG01 = "bww-qa01";
    public static final String SNC_UAT02 = "snc-uat02";
    public static final String SNC_STG01 = "snc-qa01";
    public static final List<String> ENVIRONMENTS = List.of("arb-uat01", "arb-qa01", "bww-uat02", "bww-qa01", "snc-uat02", "snc-qa01");
    public static final List<String> HEADERS = List.of("App Name", "arb-uat01", "arb-qa01", "bww-uat02", "bww-qa01", "snc-uat02", "snc-qa01", "Comparison");
}
