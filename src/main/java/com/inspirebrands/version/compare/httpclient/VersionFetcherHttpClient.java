package com.inspirebrands.version.compare.httpclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspirebrands.version.compare.model.Facet;
import com.inspirebrands.version.compare.model.NewRelicResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.inspirebrands.version.compare.constants.VersionCompareConstants.ACCOUNT_LOWER;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.ACCOUNT_PROD;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.APP_NAME_HEADER;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.ENCODED_KEY_LOWER;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.ENCODED_KEY_PROD;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.PRD01;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.STG01;

@AllArgsConstructor
@Component
public class VersionFetcherHttpClient {
    private final ObjectMapper mapper;

    public Map<String, String> getVersionsFromNewRelic(String environment) throws IOException, InterruptedException {
        Map<String, String> versions = new LinkedHashMap<>();
        versions.put(APP_NAME_HEADER, environment);
        String account = (environment.contains(STG01) || environment.contains(PRD01)) ? ACCOUNT_PROD : ACCOUNT_LOWER;
        String encodedKey = (environment.contains(STG01) || environment.contains(PRD01)) ? ENCODED_KEY_PROD : ENCODED_KEY_LOWER;

        Base64.Decoder dec = Base64.getDecoder();
        String decodedKey = new String(dec.decode(encodedKey));

        String query1 = String.format("SELECT  latest(isReady) FROM K8sPodSample WHERE namespace IN " +
                "('domain-services-v3', 'webapp', 'experience-service', 'mobile') and  label.environment ='%s' " +
                "FACET label.service, label.revision LIMIT  MAX SINCE 5 minutes AGO ORDER BY label.service ", environment);

        try {
            HttpClient httpClient = HttpClient.newHttpClient();

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://insights-api.newrelic.com/v1/accounts/" + account +
                            "/query?nrql=" + URLEncoder.encode(query1, StandardCharsets.UTF_8)))
                    .header("Accept", "application/json")
                    .header("X-Query-Key", decodedKey)
                    .build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            String responseBody = response.body();
            NewRelicResponse newRelicResponse = mapper.readValue(responseBody, NewRelicResponse.class);
            for (Facet facet : newRelicResponse.getFacets()) {
                String[] name = facet.getName();
                versions.put(name[0], name[1]);
            }
        } catch (IOException | InterruptedException e) {
            throw e;
        }
        return versions;
    }
}
