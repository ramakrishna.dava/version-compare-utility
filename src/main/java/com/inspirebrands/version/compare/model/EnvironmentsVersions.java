package com.inspirebrands.version.compare.model;

import lombok.Data;

@Data
public class EnvironmentsVersions {

    private String arbUat01;
    private String arbStg01;
    private String bwwUat02;
    private String bwwStg01;
    private String sncUat02;
    private String sncStg01;
    private boolean versionMismatched;

}