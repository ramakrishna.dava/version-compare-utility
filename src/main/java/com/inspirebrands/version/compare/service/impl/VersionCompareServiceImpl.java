package com.inspirebrands.version.compare.service.impl;

import com.inspirebrands.version.compare.httpclient.VersionFetcherHttpClient;
import com.inspirebrands.version.compare.model.EnvironmentsVersions;
import com.inspirebrands.version.compare.service.VersionCompareService;
import lombok.AllArgsConstructor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.inspirebrands.version.compare.constants.VersionCompareConstants.APP_NAME_HEADER;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.ARB_STG01;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.ARB_UAT01;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.BWW_STG01;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.BWW_UAT02;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.ENVIRONMENTS;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.HEADERS;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.SNC_STG01;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.SNC_UAT02;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.THREAD_POOL_SIZE;
import static com.inspirebrands.version.compare.constants.VersionCompareConstants.VERSION_COMPARE;

@AllArgsConstructor
@Service
public class VersionCompareServiceImpl implements VersionCompareService {
    private static final Logger logger = LoggerFactory.getLogger(VersionCompareServiceImpl.class);
    private final VersionFetcherHttpClient versionFetcherHttpClient;

    @Override
    public ByteArrayOutputStream getVersionComparisonReport() {

        long startTime = System.currentTimeMillis();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        List<Map<String, String>> allBrandVersions = new ArrayList<>();
        Map<String, EnvironmentsVersions> allEnvironments = new LinkedHashMap<>();

        logger.info("---------------------------------------------------------------------------------------");
        try {
            logger.info("Version Compare Report Generation is In-Progress");

            getVersionsFromNewRelic(allBrandVersions);
            appNameVsAllEnvVersionsData(allBrandVersions, allEnvironments);
            removeServicesFromReport(allEnvironments, "mock", "oa-webapp", "choice-", "order-tally-service", "order-fees-service", "order-validation-service", "notification-service-v0", "notification-send-service", "twilio-notification-adapter", "sfmc-notification-adapter", "auth0-adapter", "customer-domain-service", "customer-preferences-service", "customer-preferences-retrieval-service");
            generateFinalExcelReport(allEnvironments, outputStream);

            long endTime = System.currentTimeMillis();
            long executionTime = endTime - startTime;

            logger.info("Report Successfully Generated to Excel in {} seconds", executionTime / 1000);
        } catch (Exception e) {
            logger.error("Exception occurred while generating the report", e);
            createErrorSheet(outputStream, e.getMessage());
            return outputStream;
        }

        logger.info("---------------------------------------------------------------------------------------");
        return outputStream;
    }

    //Fetching version info by calling NewRelic insights API
    private void getVersionsFromNewRelic(List<Map<String, String>> allBrandVersions) throws ExecutionException, InterruptedException {

        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        List<Future<Integer>> futures = new ArrayList<>();

        for (String environment : ENVIRONMENTS) {
            final int[] index = {0};
            Callable<Integer> task = () -> {
                allBrandVersions.add(versionFetcherHttpClient.getVersionsFromNewRelic(environment));
                return index[0]++;
            };
            futures.add(executorService.submit(task));
        }
        executorService.shutdown();
        for (Future<Integer> future : futures) {
            future.get();
        }
    }

    //Returns a Map (key=App Name, value=version) for requested environments
    private void appNameVsAllEnvVersionsData(List<Map<String, String>> allBrandVersions, Map<String, EnvironmentsVersions> allEnvironments) {
        Set<String> allAppNames = new TreeSet<>();
        allBrandVersions.forEach(item -> allAppNames.addAll(item.keySet()));
        allAppNames.remove(APP_NAME_HEADER);
        allAppNames.forEach(appName -> {
            EnvironmentsVersions environmentsVersions = new EnvironmentsVersions();
            allBrandVersions.forEach(brandEnv -> {
                String envName = brandEnv.get(APP_NAME_HEADER);
                switch (envName) {
                    case ARB_UAT01 -> environmentsVersions.setArbUat01(brandEnv.get(appName));
                    case ARB_STG01 -> environmentsVersions.setArbStg01(brandEnv.get(appName));
                    case BWW_UAT02 -> environmentsVersions.setBwwUat02(brandEnv.get(appName));
                    case BWW_STG01 -> environmentsVersions.setBwwStg01(brandEnv.get(appName));
                    case SNC_UAT02 -> environmentsVersions.setSncUat02(brandEnv.get(appName));
                    case SNC_STG01 -> environmentsVersions.setSncStg01(brandEnv.get(appName));
                }
            });
            allEnvironments.put(appName, environmentsVersions);
        });
    }


    private void removeServicesFromReport(Map<String, EnvironmentsVersions> allEnvironments, String... keywords) {
        allEnvironments.entrySet().removeIf(entry -> Arrays.stream(keywords).anyMatch(keyword -> entry.getKey().toLowerCase().contains(keyword)));
    }

    //removed mock-services & oa-webapp from version comparison report
    private void removeMockAndOaWebappServicesFromReport(Map<String, EnvironmentsVersions> allEnvironments) {
        allEnvironments.entrySet().removeIf(entry -> entry.getKey().toLowerCase().contains("mock") || entry.getKey().toLowerCase().contains("oa-webapp"));
    }

    //removed choice services from version comparison report
    private void removeChoiceServicesFromReport(Map<String, EnvironmentsVersions> allEnvironments) {
        allEnvironments.entrySet().removeIf(entry -> entry.getKey().toLowerCase().contains("choice-"));
    }

    //removed consolidated services from version comparison report
    private void removeConsolidatedServicesFromReport(Map<String, EnvironmentsVersions> allEnvironments) {
        allEnvironments.entrySet().removeIf(entry -> entry.getKey().toLowerCase().contains("order-tally-service") || entry.getKey().toLowerCase().contains("order-fees-service") || entry.getKey().toLowerCase().contains("order-validation-service")
        || entry.getKey().toLowerCase().contains("notification-service-v0") || entry.getKey().toLowerCase().contains("notification-send-service")
                || entry.getKey().toLowerCase().contains("twilio-notification-adapter") || entry.getKey().toLowerCase().contains("sfmc-notification-adapter")
                || entry.getKey().toLowerCase().contains("auth0-adapter") || entry.getKey().toLowerCase().contains("customer-domain-service")
                || entry.getKey().toLowerCase().contains("customer-preferences-service") || entry.getKey().toLowerCase().contains("customer-preferences-retrieval-service"));
    }

    //Generates the final Excel report with version comparisons
    private void generateFinalExcelReport(Map<String, EnvironmentsVersions> allEnvironments, ByteArrayOutputStream outputStream) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(VERSION_COMPARE);
        createFirstRowWithEmailContent(workbook, sheet);
        createExcelHeaders(sheet);
        int rowNo = 2;

        // Create a default cell style with borders for all rows
        CellStyle cellBorderedStyle = workbook.createCellStyle();
        createBorderedStyle(cellBorderedStyle);

        for (HashMap.Entry<String, EnvironmentsVersions> entry : allEnvironments.entrySet()) {
            EnvironmentsVersions environments = entry.getValue();
            XSSFRow row = sheet.createRow(rowNo++);

            // Apply bordered style to all cells in the row
            for (int i = 0; i < HEADERS.size(); i++) {
                row.createCell(i).setCellStyle(cellBorderedStyle);
            }

            int cellNo = 0;
            row.getCell(cellNo++).setCellValue(entry.getKey());
            row.getCell(cellNo++).setCellValue(environments.getArbUat01() == null ? "-" : environments.getArbUat01());
            row.getCell(cellNo++).setCellValue(environments.getArbStg01() == null ? "-" : environments.getArbStg01());
            row.getCell(cellNo++).setCellValue(environments.getBwwUat02() == null ? "-" : environments.getBwwUat02());
            row.getCell(cellNo++).setCellValue(environments.getBwwStg01() == null ? "-" : environments.getBwwStg01());
            row.getCell(cellNo++).setCellValue(environments.getSncUat02() == null ? "-" : environments.getSncUat02());
            row.getCell(cellNo++).setCellValue(environments.getSncStg01() == null ? "-" : environments.getSncStg01());
            boolean versionCompareFlag = isVersionMatched(environments);
            row.getCell(cellNo).setCellValue(versionCompareFlag);
            if (!versionCompareFlag) {
                setColorToTextInRow(workbook, row, IndexedColors.RED1);
            }
        }
        workbook.write(outputStream);
        workbook.close();
    }

    private void createBorderedStyle(CellStyle cellBorderedStyle) {
        cellBorderedStyle.setBorderTop(BorderStyle.THIN);
        cellBorderedStyle.setBorderRight(BorderStyle.THIN);
        cellBorderedStyle.setBorderBottom(BorderStyle.THIN);
        cellBorderedStyle.setBorderLeft(BorderStyle.THIN);
    }

    //compare all environment versions for particular App name and return true if all are same else false
    private boolean isVersionMatched(EnvironmentsVersions environments) {
        List<String> fieldsToCompare = Arrays.asList(
                environments.getArbUat01(),
                environments.getArbStg01(),
                environments.getBwwUat02(),
                environments.getBwwStg01(),
                environments.getSncUat02(),
                environments.getSncStg01()
        );

        return fieldsToCompare.stream()
                .filter(Objects::nonNull)
                .distinct()
                .allMatch(field -> fieldsToCompare.stream()
                        .filter(Objects::nonNull)
                        .distinct()
                        .allMatch(field::equalsIgnoreCase));
    }

    // Create first row with timestamp
    private void createFirstRowWithEmailContent(XSSFWorkbook workbook, XSSFSheet sheet) {
        XSSFRow firstRow = sheet.createRow(0);

        // Get current date and time in system default, then convert to EST
        LocalDateTime currentTime = LocalDateTime.now();
        ZonedDateTime estTime = currentTime.atZone(ZoneId.of("America/New_York"));

        // Format the time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
        String formattedTime = estTime.format(formatter);

        XSSFCell cell = firstRow.createCell(0);
        cell.setCellValue("Timestamp: " + formattedTime);
        CellStyle style = workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        createBorderedStyle(style);
        cell.setCellStyle(style);
        sheet.addMergedRegion(CellRangeAddress.valueOf("A1:H1"));
    }

    // Create headers row for Excel
    private void createExcelHeaders(XSSFSheet sheet) {
        XSSFRow header = sheet.createRow(1);

        for (int i = 0; i < HEADERS.size(); i++) {
            header.createCell(i).setCellValue(HEADERS.get(i));
        }

        setBgColorToRow(header, IndexedColors.LIGHT_YELLOW, FillPatternType.SOLID_FOREGROUND);
    }

    //Set colour to text in Excel row
    private void setColorToTextInRow(XSSFWorkbook workbook, XSSFRow row, IndexedColors indexedColors) {
        Font font = workbook.createFont();
        font.setColor(indexedColors.getIndex());

        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        createBorderedStyle(style);

        row.forEach(cell -> cell.setCellStyle(style));
    }

    //Set background colour to Excel row
    private void setBgColorToRow(XSSFRow row, IndexedColors indexedColors, FillPatternType fillPatternType) {
        XSSFWorkbook workbook = row.getSheet().getWorkbook();

        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(indexedColors.getIndex());
        style.setFillPattern(fillPatternType);
        createBorderedStyle(style);

        row.forEach(cell -> cell.setCellStyle(style));
    }

    private void createErrorSheet(ByteArrayOutputStream outputStream, String errorMessage) {
        try (XSSFWorkbook errorBook = new XSSFWorkbook()) {
            XSSFSheet errorSheet = errorBook.createSheet("Error");
            XSSFRow errorRow = errorSheet.createRow(0);
            errorRow.createCell(0).setCellValue("Error occurred while generating the report. Error details: " + errorMessage);
            errorBook.write(outputStream);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}