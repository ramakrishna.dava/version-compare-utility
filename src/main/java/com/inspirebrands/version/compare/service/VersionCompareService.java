package com.inspirebrands.version.compare.service;

import java.io.ByteArrayOutputStream;

public interface VersionCompareService {
    ByteArrayOutputStream getVersionComparisonReport();
}